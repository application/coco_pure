#include "welcome.h"

struct _CocoWelcome
{
  AdwBin parent_instance;
};

G_DEFINE_TYPE (CocoWelcome, coco_welcome, ADW_TYPE_BIN)

static void
coco_welcome_class_init (CocoWelcomeClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/cc/xuthus/coco_pure/components/welcome/welcome.ui");
}

static void
coco_welcome_init (CocoWelcome *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

