#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define COCO_TYPE_WELCOME (coco_welcome_get_type())

G_DECLARE_FINAL_TYPE (CocoWelcome, coco_welcome, COCO, WELCOME, AdwBin)

G_END_DECLS

