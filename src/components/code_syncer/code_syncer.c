#include "code_syncer.h"

struct _CocoCodeSyncer
{
  AdwBin parent_instance;
};

G_DEFINE_TYPE (CocoCodeSyncer, coco_code_syncer, ADW_TYPE_BIN)

static void
coco_code_syncer_class_init (CocoCodeSyncerClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/cc/xuthus/coco_pure/components/code_syncer/code_syncer.ui");
}

static void
coco_code_syncer_init (CocoCodeSyncer *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

