#include "password.h"

struct _CocoPassword
{
  AdwBin parent_instance;
};

G_DEFINE_TYPE (CocoPassword, coco_password, ADW_TYPE_BIN)

static void
coco_password_class_init (CocoPasswordClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/cc/xuthus/coco_pure/components/password/password.ui");
}

static void
coco_password_init (CocoPassword *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

