#include "time_convert.h"

struct _CocoTimeConvert
{
  AdwBin parent_instance;
};

G_DEFINE_TYPE (CocoTimeConvert, coco_time_convert, ADW_TYPE_BIN)

static void
coco_time_convert_class_init (CocoTimeConvertClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/cc/xuthus/coco_pure/components/time_convert/time_convert.ui");
}

static void
coco_time_convert_init (CocoTimeConvert *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

