#include "hasher.h"

struct _CocoHasher
{
  AdwBin parent_instance;
};

G_DEFINE_TYPE (CocoHasher, coco_hasher, ADW_TYPE_BIN)

static void
coco_hasher_class_init (CocoHasherClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/cc/xuthus/coco_pure/components/hasher/hasher.ui");
}

static void
coco_hasher_init (CocoHasher *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

