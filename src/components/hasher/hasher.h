#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define COCO_TYPE_HASHER (coco_hasher_get_type())

G_DECLARE_FINAL_TYPE (CocoHasher, coco_hasher, COCO, HASHER, AdwBin)

G_END_DECLS

