#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define COCO_TYPE_CODE_SYNCER (coco_code_syncer_get_type())

G_DECLARE_FINAL_TYPE (CocoCodeSyncer, coco_code_syncer, COCO, CODE_SYNCER, AdwBin)

G_END_DECLS

