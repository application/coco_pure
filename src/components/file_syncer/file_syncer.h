#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define COCO_TYPE_FILE_SYNCER (coco_file_syncer_get_type())

G_DECLARE_FINAL_TYPE (CocoFileSyncer, coco_file_syncer, COCO, FILE_SYNCER, AdwBin)

G_END_DECLS

