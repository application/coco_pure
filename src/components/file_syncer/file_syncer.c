#include "file_syncer.h"

struct _CocoFileSyncer
{
  AdwBin parent_instance;
};

G_DEFINE_TYPE (CocoFileSyncer, coco_file_syncer, ADW_TYPE_BIN)

static void
coco_file_syncer_class_init (CocoFileSyncerClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/cc/xuthus/coco_pure/components/file_syncer/file_syncer.ui");
}

static void
coco_file_syncer_init (CocoFileSyncer *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

